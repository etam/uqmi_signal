uqmi signal page for LEDE/OpenWrt
=================================

This is a html page for LEDE/OpenWrt, which gets lte signal info from uqmi and displays as a graph.

Installaton
-----------

  * wget http://github.com/joewalnes/smoothie/raw/master/smoothie.js -O signal/smoothie.js
  * scp -r cgi-bin signal 192.168.1.1:/www

Usage
-----

Open https://192.168.1.1/signal
