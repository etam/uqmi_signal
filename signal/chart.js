var intervalSecs = 10;


var snrChart = new SmoothieChart({
    millisPerPixel: 100,
    grid: {
        fillStyle: '#ffffff',
        millisPerLine: 10000,
        verticalSections: 0,
    },
    labels: {
        precision: 0,
        fillStyle: '#000000',
    },
    timestampFormatter: SmoothieChart.timeFormatter,
    minValue: 0,
    horizontalLines: [
        {
            color: '#ff0000',
            value: 0,
        },{
            color: '#0000ff',
            value: 13,
        },{
            color: '#00ff00',
            value: 20,
        }
    ]
});
var snrCanvas = document.getElementById('snr-smoothie-chart');
var snrSeries = new TimeSeries();
snrChart.addTimeSeries(snrSeries, {
    lineWidth: 2,
    strokeStyle: '#00ff00',
});
snrChart.streamTo(snrCanvas, intervalSecs*1000);


var rsrpChart = new SmoothieChart({
    millisPerPixel: 100,
    grid: {
        fillStyle: '#ffffff',
        millisPerLine: 10000,
        verticalSections: 0,
    },
    labels: {
        precision: 0,
        fillStyle: '#000000',
    },
    timestampFormatter: SmoothieChart.timeFormatter,
    minValue: -140,
    maxValue: -44,
    horizontalLines: [
        {
            color: '#ff0000',
            value: -100,
        },{
            color: '#0000ff',
            value: -90,
        },{
            color: '#00ff00',
            value: -80,
        }
    ]
});
var rsrpCanvas = document.getElementById('rsrp-smoothie-chart');
var rsrpSeries = new TimeSeries();
rsrpChart.addTimeSeries(rsrpSeries, {
    lineWidth: 2,
    strokeStyle: '#ff0000',
});
rsrpChart.streamTo(rsrpCanvas, intervalSecs*1000);


var rsrqChart = new SmoothieChart({
    millisPerPixel: 100,
    grid: {
        fillStyle: '#ffffff',
        millisPerLine: 10000,
        verticalSections: 0,
    },
    labels: {
        precision: 0,
        fillStyle: '#000000',
    },
    timestampFormatter: SmoothieChart.timeFormatter,
    minValue: -20,
    maxValue: -3,
    horizontalLines: [
        {
            color: '#ff0000',
            value: -20,
        },{
            color: '#0000ff',
            value: -15,
        },{
            color: '#00ff00',
            value: -10,
        }
    ]
});
var rsrqCanvas = document.getElementById('rsrq-smoothie-chart');
var rsrqSeries = new TimeSeries();
rsrqChart.addTimeSeries(rsrqSeries, {
    lineWidth: 2,
    strokeStyle: '#0000ff',
});
rsrqChart.streamTo(rsrqCanvas, intervalSecs*1000);


var rssiChart = new SmoothieChart({
    millisPerPixel: 100,
    grid: {
        fillStyle: '#ffffff',
        millisPerLine: 10000,
        verticalSections: 0,
    },
    labels: {
        precision: 0,
        fillStyle: '#000000',
    },
    timestampFormatter: SmoothieChart.timeFormatter,
});
var rssiCanvas = document.getElementById('rssi-smoothie-chart');
var rssiSeries = new TimeSeries();
rssiChart.addTimeSeries(rssiSeries, {
    lineWidth: 2,
    strokeStyle: '#000000',
});
rssiChart.streamTo(rssiCanvas, intervalSecs*1000);


function update(data) {
    var timestamp = new Date().getTime();
    snrSeries.append(timestamp, data.snr);
    rsrpSeries.append(timestamp, data.rsrp);
    rsrqSeries.append(timestamp, data.rsrq);
    rssiSeries.append(timestamp, data.rssi);
}

function refresh() {
    fetch('/cgi-bin/signal')
        .then((response) => {
            return response.json();
        })
        .then((myJson) => {
            update(myJson);
        });
    setTimeout(refresh, intervalSecs*1000);
}

refresh();
